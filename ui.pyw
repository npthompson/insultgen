import html

import wx
from insult import InsultApi
from insult2 import InsultGenerator
from dadjoke import DadJoke
from yomama import YoMama
from engine import ApiException

class MainPanel(wx.Panel):

    def __init__(self,parent):
        super().__init__(parent)

        self.insult_api = InsultApi()
        self.insult_generator = InsultGenerator()
        self.dad_joke = DadJoke()
        self.yo_mama = YoMama()

        self.sources = {
            "Insult generator": self.insult_generator.get,
            "Insult API":self.insult_api.get,
            "Dad Joke":self.dad_joke.get,
            "Yo Mama": self.yo_mama.get
        }

        choices = [str(x) for x in self.sources.keys()]
        main_sizer = wx.BoxSizer(wx.VERTICAL)
        button_sizer = wx.BoxSizer(wx.HORIZONTAL)

        self.choose_generator = wx.Choice(self,id=wx.ID_ANY,choices=choices)
        self.choose_generator.SetSelection(0)
        main_sizer.Add(self.choose_generator,0,wx.ALL|wx.CENTER,5)

        self.result_text = wx.TextCtrl(self,style=wx.TE_MULTILINE|wx.TE_READONLY)
        main_sizer.Add(self.result_text,-1,wx.ALL|wx.EXPAND,5)

        generate_button = wx.Button(self,id=wx.ID_ANY,label="&Generate")
        generate_button.Bind(wx.EVT_BUTTON,self.on_generate)
        button_sizer.Add(generate_button,0,wx.CENTER,5)

        copy_button = wx.Button(self,wx.ID_ANY,label="&Copy")
        copy_button.Bind(wx.EVT_BUTTON,self.on_copy)
        button_sizer.Add(copy_button,0,wx.CENTER,5)

        close_button = wx.Button(self,wx.ID_ANY,label="Clos&e")
        close_button.Bind(wx.EVT_BUTTON,self.on_close)
        button_sizer.Add(close_button,0,wx.CENTER,5)

        main_sizer.Add(button_sizer,0,wx.ALL|wx.CENTER,5)

        self.SetSizer(main_sizer)

    def on_generate(self,event):
        choice = self.choose_generator.GetString(self.choose_generator.GetCurrentSelection())
        self.result_text.SetValue("Please wait...")
        try:
            result = html.unescape(self.sources[choice]())
        except ApiException as e:
            result = f"Error: {e.reason}"
        self.result_text.SetValue(result)

    def on_copy(self,event):
        self.result_text.SetFocus()
        self.result_text.SelectAll()
        self.result_text.Copy()

    def on_close(self,event):
        self.Parent.Close()


class AppFrame(wx.Frame):
    
    def __init__(self,title="Insult and Joke Generator"):
        super().__init__(None,title=title,size=(600,400))

        main_panel = MainPanel(self)


if __name__ == "__main__":
    app = wx.App()
    frame = AppFrame()
    frame.Show()
    app.MainLoop()