import requests
from engine import Engine, ApiException

class InsultApi(Engine):
    def get(self):
        res = requests.get("https://evilinsult.com/generate_insult.php?lang=en&type=json")
        if res.ok:
            data =  res.json()
            insult = data["insult"]
            return(insult)
        else:
            raise ApiException("Error with API {}".format(res.reason))


if __name__ == "__main__":
    # test
    api = InsultApi()
    print(api.get())
    input("Press enter to close...")

