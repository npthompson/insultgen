from unittest import TestCase
from engine import Engine, ApiException
import requests

class YoMama(Engine):
    def get(self):
        # GET https://yomomma-api.herokuapp.com/jokes
        # gives a dict: {"joke": f"{Joke text}"}
        response = requests.get("https://yomomma-api.herokuapp.com/jokes")
        if response.ok:
            joke = response.json()["joke"]
            return joke
        else:
            raise ApiException(f"{response.status_code}: {response.reason}")

class TestYoMama(TestCase):
    def test_engine(self):
        engine = YoMama()
        response = engine.get()
        self.assertTrue(response)
        self.assertIsInstance(response,str)
