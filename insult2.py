import random
import json
from pathlib import Path

class InsultGenerator():
        def __init__(self):
                jsonfile = Path.cwd().joinpath("insult.json")
                jsondata = json.loads(jsonfile.read_text())
                first = jsondata["first"]
                second = jsondata["second"]
                third = jsondata["third"]


                self.insult_groups = [first,second,third]

        def get(self):
                result = ""
                for group in self.insult_groups:
                        result += random.choice(group) + " "

                return result

if __name__ == "__main__":
        generator = InsultGenerator()
        print(generator.get())
        input("Press enter to continue...")