import sys
from requests import get

class DadJoke:
    url = r"https://icanhazdadjoke.com/"
    headers = {"Accept":"application/json"}

    def get(self):
        res = get(DadJoke.url,headers=DadJoke.headers)
        if not res.ok:
            print(f"{res.status_code}: {res.reason}")
            return
        d = res.json()
        joke = d["joke"]
        return joke

if __name__ == "__main__":
    
    d = DadJoke()
    print(d.get())
    if not "-q" in sys.argv:
        input("Press enter to close.")
